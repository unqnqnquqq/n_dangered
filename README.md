# Graduate work



## N_dangered

We are developing a mobile game to motivate people to learn more about the current ecological situation in Ukraine, about animals from the Red and Black Books.

We use anthropomorphic characters to create a more personalized game experience and increase empathy for animals.

We want to teach people to create favorable conditions for the life of animals and for people to get used to the idea of ​​coexistence of people and animals, and therefore to remember the obligation to maintain the standard of living of all living things on Earth.


## Concept

Genre: Turn-based strategy / RPG

Target audience:

Age: 12-35

Number of players: single player, with the ability to interact with other players

Name: comes from the word Endangered - under threat of extinction