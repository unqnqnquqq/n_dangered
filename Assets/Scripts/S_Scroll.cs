using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_Scroll : MonoBehaviour
{
    public S_TouchManager touchManager;
    public GameObject scrollGameObject;
    private float prevvY = -1;
    private bool isFirst = true;
    private void OnEnable()
    {   
        touchManager.OnDrag += TouchManager_OnDrag;
        touchManager.OnDragCancelled += TouchManager_OnDragCancelled; 
    }
    private void TouchManager_OnDragCancelled(object sender, System.EventArgs e)
    {   
        isFirst = true; 
    }
    private void OnDisable()
    {   
        touchManager.OnDrag -= TouchManager_OnDrag;
        touchManager.OnDragCancelled -= TouchManager_OnDragCancelled; 
    }
    private void TouchManager_OnDrag(object sender, Vector3 e)
    {   
        if (!isFirst) 
        { 
            scrollGameObject.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0, (prevvY - e.y) * 30); 
        }
        else
        { 
            isFirst = false; 
        }
        Debug.Log(prevvY - e.y);
        prevvY = e.y; 
    }
}