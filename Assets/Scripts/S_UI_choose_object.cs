using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class S_UI_choose_object : MonoBehaviour
{
    public Sprite ActiveSprite, UnactiveSprite;
    public Button fon_icon;
    public Image icon;
    public event EventHandler on_click;
    public virtual void ChooseObject(int level)
    { 
        on_click?.Invoke(this, EventArgs.Empty); 
    }
    private void OnEnable()
    { fon_icon = GetComponent<Button>();
        icon = GetComponent<Image>();
        fon_icon.onClick.AddListener(() => { icon.sprite = ActiveSprite; }); }
    public virtual void UnChooseObject()
    {
        icon.sprite = UnactiveSprite;
    }
}