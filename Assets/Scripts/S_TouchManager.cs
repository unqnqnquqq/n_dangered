using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;

public class S_TouchManager : MonoBehaviour
{
    public event EventHandler<Vector3> OnDrag;
    public event EventHandler OnDragCancelled;
    public event EventHandler OnTap;
    private PlayerInput playerInput;
    private InputAction dragAction;
    private InputAction tapAction;
    private void Awake()
    {   
        playerInput = GetComponent<PlayerInput>();
        dragAction = playerInput.actions["Drag"];
        tapAction = playerInput.actions["Tap"]; 
    }
    private void OnEnable()
    {   
        EnhancedTouchSupport.Enable();
        dragAction.performed += DragPerformed;
        tapAction.performed += OnTapPerformed;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp += StopTouch; 
    }
    private void OnDisable()
    {   
        dragAction.performed -= DragPerformed;
        tapAction.performed -= OnTapPerformed;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp -= StopTouch; 
    }
    private void DragPerformed(InputAction.CallbackContext context)
    {   
        Vector3 position = Camera.main.ScreenToWorldPoint(context.ReadValue<Vector2>());
        position.z = 0.0f;
        OnDrag?.Invoke(this, position); 
    }
        private void StopTouch(Finger fin)
    {   
        OnDragCancelled?.Invoke(this, EventArgs.Empty); 
    } 
    private void OnTapPerformed(InputAction.CallbackContext context)
    {
        OnTap?.Invoke(this, EventArgs.Empty);
    }
}