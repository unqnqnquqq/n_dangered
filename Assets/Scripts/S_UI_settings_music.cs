using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class S_UI_settings_music : MonoBehaviour
{
    public Sprite ActiveSprite, UnactiveSprite;
    public Button setings_music;
    public Image icon;
    private void OnEnable()
    {   
        setings_music = GetComponent<Button>();
        setings_music.onClick.AddListener(() =>
        {   
            if (IsEnable) 
            { 
                icon.sprite = ActiveSprite; 
            }
            else 
            { 
                icon.sprite = UnactiveSprite; 
            }
            IsEnable = !IsEnable;
        });
    }
    private bool IsEnable = false;
}