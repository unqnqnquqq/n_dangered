using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Button_Audio : MonoBehaviour
{
    public AudioSource button_AudioSource;
    public AudioClip audio_Clip;
    public S_TouchManager touch_Manager;
    public bool muted = true;
    private void OnEnable()
    {
        touch_Manager.OnTap += Touch_Manager_OnTap;
    }
    private void OnDisable()
    {
        touch_Manager.OnTap -= Touch_Manager_OnTap;
    }
    private void Touch_Manager_OnTap(object sender, System.EventArgs e)
    {
        Play_Sound();
    }
    public void Play_Sound()
    {
        button_AudioSource.PlayOneShot(audio_Clip);
    }
    public void mute_sound()
    {
        button_AudioSource.mute = muted;
        muted = !muted;
    }
}