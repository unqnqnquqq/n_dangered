using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public bool muted = true;
    public AudioSource music_AudioSource;
    public void mute_sound()
    {
        music_AudioSource.mute = muted;
        muted = !muted;
    }
}