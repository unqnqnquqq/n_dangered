using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class S_UI_choose_unit : MonoBehaviour
{
    public List<S_UI_choose_object> gameObjects = new List<S_UI_choose_object>();
    public virtual void Start()
    {
        foreach (S_UI_choose_object go in gameObjects)
        {
            go.on_click += S_UI_choose_object_on_click;
        }
    }

    public virtual void S_UI_choose_object_on_click(object sender, System.EventArgs e)
    {
        foreach (S_UI_choose_object go in gameObjects)
        {
            if (go != sender) go.UnChooseObject();
        }
    }
}