using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class S_UI_settings_yes_restart : MonoBehaviour
{
    [SerializeField] private GameObject settings;
    [SerializeField] private GameObject check_restart;
    [SerializeField] private TextMeshProUGUI energy;
    [SerializeField] private TextMeshProUGUI money;
    [SerializeField] private TextMeshProUGUI crystals;
    public void Restart()
    {   
        settings.SetActive(!settings.activeSelf);
        check_restart.SetActive(!check_restart.activeSelf);
        energy.text = "0";
        money.text = "0";
        crystals.text = "0"; 
    }
}