using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_UI_ShowClose_window : MonoBehaviour
{
    [SerializeField] private GameObject window1;
    [SerializeField] private GameObject window2;
    public void ShowCloseWindow() 
    { 
        window1.SetActive(!window1.activeSelf);
        window2.SetActive(!window2.activeSelf); 
    }
}