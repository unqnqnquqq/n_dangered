using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_UI_close_window : MonoBehaviour
{
    [SerializeField] private GameObject window;
    public void CloseWindow()
    {   
        window.SetActive(!window.activeSelf); 
    }
}